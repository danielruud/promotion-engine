#ifndef PROMOTION_ENGINE_H_
#define PROMOTION_ENGINE_H_

#include <unordered_map>
#include <vector>

namespace engine {

using SKUID = char;

struct Cart {
  public:
    Cart(std::vector<SKUID> cart_products) : 
        products(cart_products) {}

    const std::vector<SKUID> products;
};

struct StockUnit {
  public:
    StockUnit(SKUID id, int32_t unit_price) : 
        skuid(id), price(unit_price) {}
  
    const SKUID skuid;
    const int32_t price;
};

struct PromotionRule {
  public:
    PromotionRule(std::vector<std::pair<SKUID, int32_t>> promotion_bundle, int32_t promotion_price) : 
        bundle(promotion_bundle), price(promotion_price) {}

    const std::vector<std::pair<SKUID, int32_t>> bundle;
    const int32_t price;
};

/**
 * Promotion engine that calculates the order value of a cart given stock unit prices
 * and optional promotion rules.
 * 
 * Promotion rules are applied in greedy fashion from newest to oldest.
 */ 
class PromotionEngine {
  public:
    PromotionEngine(std::vector<StockUnit> units);
    PromotionEngine(std::vector<StockUnit> units, std::vector<PromotionRule> promotion_rules);

    void AddStockUnit(StockUnit unit);
    void AddPromotionRule(PromotionRule promotion_rule);

    int64_t CalculateOrderValue(Cart cart) noexcept(false);

  private:
    std::unordered_map<SKUID, int32_t> units_;
    std::vector<PromotionRule> promotion_rules_;
};

} // namespace engine

#endif // PROMOTION_ENGINE_H_