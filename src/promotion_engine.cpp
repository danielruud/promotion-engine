#include "promotion_engine.h"

#include <string>

namespace engine {

PromotionEngine::PromotionEngine(std::vector<StockUnit> units) :
    PromotionEngine(units, {}) {}

PromotionEngine::PromotionEngine(std::vector<StockUnit> units, std::vector<PromotionRule> promotion_rules) : 
    promotion_rules_(promotion_rules) {
  for (auto& unit : units) {
    units_[unit.skuid] = unit.price;
  }
}

void PromotionEngine::AddStockUnit(StockUnit unit) {
  // Overwrites the existing price if SKU exists.
  units_[unit.skuid] = unit.price;
}

void PromotionEngine::AddPromotionRule(PromotionRule promotion_rule) {
  promotion_rules_.push_back(promotion_rule);
}

// Complexity: O(max(number of rules, number of products in the cart)).
int64_t PromotionEngine::CalculateOrderValue(Cart cart) noexcept(false) {
  std::unordered_map<SKUID, int32_t> processed_cart;

  // Count the number of units of each product.
  for (auto& skuid : cart.products) {
    // Note: If 'skuid' does not exists in the map, it is added automatically
    // with the default value of '0'. 
    processed_cart[skuid] += 1;
  }

  int64_t order_value = 0;  
  // Newest rules first.
  for (int i = promotion_rules_.size() - 1; i >= 0; --i) {
    auto& rule = promotion_rules_[i];
    int index = 0;
    // The number of times the rule can be applied.
    int multiply = INT_MAX;

    // Check if the rule can be applied to the cart.
    for (; index < rule.bundle.size(); ++index) {
      auto& sku = rule.bundle[index];
      // The rule has a product that is not in the cart.
      if (processed_cart.find(sku.first) == processed_cart.end()) {
        break;
      }
      // The cart does not have enough units of the product to satisfy the rule.
      if (sku.second > processed_cart[sku.first]) {
        break;
      }
      multiply = std::min(multiply, processed_cart[sku.first] / sku.second);
    }
    // Apply the rule.
    if (index == rule.bundle.size()) {
      for (auto& sku : rule.bundle) {
        processed_cart[sku.first] -= sku.second * multiply;
        if (processed_cart[sku.first] == 0) {
          processed_cart.erase(sku.first);
        }
      }
      order_value += rule.price * multiply;
    }
  }

  // Apply the normal price to the rest of the cart.
  for (auto& sku : processed_cart) {
    // Unknown sku id.
    if (units_.find(sku.first) == units_.end()) {
      throw "SKU id " + std::to_string(sku.first) + " is not known!";
    }
    order_value += sku.second * units_[sku.first];
  }

  return order_value;;
}

} // namespace engine