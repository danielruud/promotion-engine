#define CATCH_CONFIG_MAIN
#include "../third_party/catch.hpp"

#include "../src/promotion_engine.h"

#include <vector>

namespace engine {
namespace promotion_engine_test {

/*
    Active Promotions
    -

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    1 * A
    1 * B
    1 * C

    Total		
    100
*/
TEST_CASE("No promotion available", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  PromotionEngine engine{units};

  Cart c{{'A', 'B', 'C'}};

  auto value = engine.CalculateOrderValue(c);
  REQUIRE(value == 50 + 30 + 20);  
}

/*
    Active Promotions
    2 * A   80

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    1 * A
    1 * B
    1 * C

    Total		
    100
*/
TEST_CASE("No promotion applies", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  std::vector<PromotionRule> promotion_rules{PromotionRule{{{'A', 2}}, 80}};
  PromotionEngine engine{units, promotion_rules};

  Cart c{{'A', 'B', 'C'}};

  auto value = engine.CalculateOrderValue(c);
  REQUIRE(value == 50 + 30 + 20);  
}

/*
    Active Promotions
    2 * A   80

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    2 * A
    1 * B
    1 * C

    Total		
    130
*/
TEST_CASE("Single SKU promotion", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  std::vector<PromotionRule> promotion_rules{PromotionRule{{{'A', 2}}, 80}};
  PromotionEngine engine{units, promotion_rules};

  Cart c{{'A', 'B', 'C', 'A'}};

  auto value = engine.CalculateOrderValue(c);
  REQUIRE(value == 80 + 30 + 20);
}

/*
    Active Promotions
    1 * C + 1 * D   30

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    1 * A
    1 * B
    1 * C
    1 * D

    Total		
    110
*/
TEST_CASE("Bundle promotion", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  std::vector<PromotionRule> promotion_rules{PromotionRule{{{'C', 1}, {'D', 1}}, 30}};
  PromotionEngine engine{units, promotion_rules};

  Cart c{{'A', 'B', 'C', 'D'}};

  auto value = engine.CalculateOrderValue(c);
  REQUIRE(value == 50 + 30 + 30);
}

/*
    Active Promotions
    3 * A         130
    2 * B          45
    1 * C + 1 * D  30

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    5 * A
    5 * B
    1 * C

    Total		
    370
*/
TEST_CASE("Multiple promotions - Scenario 1", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  std::vector<PromotionRule> promotion_rules{PromotionRule{{{'A', 3}}, 130}, PromotionRule{{{'B', 2}}, 45}, PromotionRule{{{'C', 1}, {'D', 1}}, 30}};
  PromotionEngine engine{units, promotion_rules};

  Cart c{{'A', 'A', 'A', 'A', 'A', 'B', 'B', 'B', 'B', 'B', 'C'}};

  auto value = engine.CalculateOrderValue(c);
  REQUIRE(value == 130 + 2*50 + 45 + 45 + 30 + 20);  
}

/*
    Active Promotions
    3 * A         130
    2 * B          45
    1 * C + 1 * D  30

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    3 * A
    5 * B
    1 * C
    1 * D

    Total		
    280
*/
TEST_CASE("Multiple promotions - Scenario 2", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  std::vector<PromotionRule> promotion_rules{PromotionRule{{{'A', 3}}, 130}, PromotionRule{{{'B', 2}}, 45}, PromotionRule{{{'C', 1}, {'D', 1}}, 30}};
  PromotionEngine engine{units, promotion_rules};

  Cart c{{'A', 'A', 'A', 'B', 'B', 'B', 'B', 'B', 'C', 'D'}};

  auto value = engine.CalculateOrderValue(c);
  REQUIRE(value == 130 + 45 + 45 + 30 + 30);  
}

/*
    Active Promotions
    3 * A         130
    2 * B          45
    1 * C + 1 * D  30
    1 * B          20

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    3 * A
    1 * B
    1 * C
    1 * D

    Total		
    180
*/
TEST_CASE("Promotion added later", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  std::vector<PromotionRule> promotion_rules{PromotionRule{{{'A', 3}}, 130}, PromotionRule{{{'B', 2}}, 45}, PromotionRule{{{'C', 1}, {'D', 1}}, 30}};
  PromotionEngine engine{units, promotion_rules};

  SECTION("Change price through promotion") {  
    // Add promotion rule to decrease price of 'B' from 30 to 20.
    engine.AddPromotionRule(PromotionRule{{{'B', 1}}, 20});
  }

  SECTION("Change price permanently") {  
    // Change price of 'B'.
    engine.AddStockUnit(StockUnit{'B', 20});
  }

  Cart c{{'A', 'B', 'C', 'D', 'A', 'A'}};

  auto value = engine.CalculateOrderValue(c);
  REQUIRE(value == 130 + 20 + 30);
}

/*
    Active Promotions
    -

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    1 * A
    1 * E

    Exception thrown due to not knowing 'E'
*/
TEST_CASE("SKU price not known", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  PromotionEngine engine{units};

  // SKU id 'E' is not known to the engine.
  Cart c{{'A', 'E'}};

  REQUIRE_THROWS(engine.CalculateOrderValue(c));
}

/*
    Active Promotions
    1 * A + 1 * E   60

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    1 * A
    1 * E

    Total		
    60
*/
TEST_CASE("Product sold only bundled with other product in a promotion", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  std::vector<PromotionRule> promotion_rules{PromotionRule{{{'A', 1}, {'E', 1}}, 60}};
  PromotionEngine engine{units, promotion_rules};

  Cart c{{'A', 'E'}};

  auto value = engine.CalculateOrderValue(c);
  REQUIRE(value == 60);
}


/*
    Active Promotions
    2 * A + 1 * B   110

    StokUnit
    1 * A	50
    1 * B	30
    1 * C	20
    1 * D   15

    Cart
    4 * A
    2 * B

    Total		
    220
*/
TEST_CASE("Promotion applied multiple times", "[order value]") {
  std::vector<StockUnit> units{StockUnit{'A', 50}, StockUnit{'B', 30}, StockUnit{'C', 20}, StockUnit{'D', 15}};
  std::vector<PromotionRule> promotion_rules{PromotionRule{{{'A', 2}, {'B', 1}}, 110}};
  PromotionEngine engine{units, promotion_rules};

  Cart c{{'A', 'A', 'A', 'A', 'B', 'B'}};

  auto value = engine.CalculateOrderValue(c);
  REQUIRE(value == 2 * 110);
}

} // namespace promotion_engine_test
} // namespace engine