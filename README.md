# C++ Promotion Engine

Promotion engine that calculates the order value of a cart given stock unit prices
and optional promotion rules.
Promotion rules are applied in greedy fashion from newest to oldest.

## Project Structure

```
src/                    # Source code of the project
test/                   # Unit tests of the project
third_party/            # Folder containing third party source code used by the project
build_and_run_tests.sh  # Bash script used to build the project and run the unit tests
README.md               # This file
```

## Building the project

In the its current form, the project can only be built for macOS.

The promotion engine is built as a shared library so it can be added easily
as a dependency to other projects.

Run:
`./build_and_run_tests.sh`