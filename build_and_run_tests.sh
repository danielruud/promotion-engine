#!/bin/bash

# Note: This script will only run on macOS!

# Build dynamic library
g++ -std=c++17 -c src/promotion_engine.cpp -o promotion_engine.o
g++ -shared -o promotion_engine.dylib promotion_engine.o

# Build tests
g++ -std=c++17 -c test/promotion_engine_test.cpp -o promotion_engine_test.o
g++ promotion_engine_test.o promotion_engine.dylib -o promotion_engine_test

# Execute tests
./promotion_engine_test
